# dotfiles
My dotfiles for linux

## Installed software

+ **bspwm** - Window manager
+ **sxhkd** - Hotkey daemon
+ **picom** - Composite manager
+ **polybar** - Status bar
+ **kitty** - Terminal emulator
+ **zsh + ohmyzsh (powerlevel10k)** - Shell
+ **rofi** - Dynamic menu
+ **dolphin** - File manager
+ **vscode** - Text editor

## GUI
+ **arc-grey-dark** - GTK theme
+ **paper** - Icon theme
+ **adwaita** - Cursor theme
